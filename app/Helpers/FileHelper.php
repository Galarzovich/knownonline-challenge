<?php


namespace App\Helpers;

use App\Enums\TipoArchivo;
use Illuminate\Http\UploadedFile;


abstract class FileHelper
{

    /**
     * Obtiene el tipo de archivo apartir de un archivo cargado
     * @param UploadedFile $file
     * @return int|TipoArchivo
     */
    public static function getTipoFile(UploadedFile $file)
    {
        $partes=explode('/',$file->getMimeType());

        if ($partes[0] === 'image'){
            return TipoArchivo::IMG;
        }

        $mystring = $file->getMimeType();
        $findme   = 'pdf';
        $pos = strpos($mystring, $findme);
        if ($pos>-1){
            return TipoArchivo::PDF;
        }
        if ($file->getClientOriginalExtension()==='doc'||$file->getClientOriginalExtension()==='docx'){
            return TipoArchivo::DOC;
        }



        return TipoArchivo::DESCONOCIDO;

    }

}
