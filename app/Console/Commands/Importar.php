<?php

namespace App\Console\Commands;

use App\Models\Clientes;
use App\Models\Pedidos;
use App\Tools\ApiMessage;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Importar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar los datos de la API en la db local';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Request $request)
    {
        $this->info("Importando datos de VTEX");

        //definimos las credenciales y la Api a consultar



        //obtenemos la fecha actual
        $now = new Carbon();
        $now = $now->format('Y-m-d');


        //generamos la consulta
        //en este caso, el ejercicio solicita la pedidos desde el principio del año, por ello lo dejo estatico,
        $pedidos = Http::withHeaders([
            'X-VTEX-API-AppToken' =>  $token,
            'X-VTEX-API-AppKey' => $key
        ])->get($api, [
            'f_creationDate' => 'creationDate:[2021-01-01T00:00:00.000Z TO '.$now.'T00:00:00.000Z]',
            'f_status' => 'ready-for-handling',
            'per_page' => '30',
        ]);

        //obtenemos el numero de paginas
        $aux = $pedidos['paging'];
        $paginas = $aux['pages'];


        //recorremos las paginas de la api, esto es porque la api de manera generica envia 15 ordenes por pagina
        //Aumente a 30 ordenes por pagina para minimizar la cantidad de consultas
        for ($i = 1; $i <= $paginas; $i++)
        {
                $pedidos = Http::withHeaders([
                    'X-VTEX-API-AppToken' =>  $token,
                    'X-VTEX-API-AppKey' => $key
                ])->get($api, [
                    'f_creationDate' => 'creationDate:[2021-01-01T00:00:00.000Z TO '.$now.'T00:00:00.000Z]',
                    'f_status' => 'ready-for-handling',
                    'per_page' => '30',
                    'page' => $i
                ]);

                foreach ($pedidos['list'] as $detalle)
                {
                    //verificamos si el pedido se encuentra almacenado en la db
                    if(Pedidos::where('orderId','=',$detalle['orderId'])->exists())
                    {
                        $this->info("El pedido ya se encuentra registrado");
                        continue;
                    }

                    //instanciamos el pedido
                    $pedidos = new Pedidos();

                    $pedidos->orderId = $detalle['orderId'];
                    $pedidos->monto_total = $detalle['totalValue'];

                    $orderId = $detalle['orderId'];

                    //consultamos a la api cual fue el id de pago que se utilizo
                    $consulta_payment = Http::withHeaders([
                        'X-VTEX-API-AppToken' =>  $token,
                        'X-VTEX-API-AppKey' => $key
                    ])->get($api.'/'.$orderId.'/payment-transaction');

                    foreach ($consulta_payment['payments'] as $detalle2)
                    {
                        $pedidos->paymentSystem = $detalle2['paymentSystem'];
                    }

                    //consultamos los datos del cliente
                    $consulta_client = Http::withHeaders([
                        'X-VTEX-API-AppToken' =>  $token,
                        'X-VTEX-API-AppKey' => $key
                    ])->get($api.'/'.$orderId);

                    //verificamos si el cliente ya se encuentra cargado, esto lo hago para que no haya registros repetidos
                    $existe = Clientes::where('userProfileId','=',$consulta_client['clientProfileData']['userProfileId'])->exists();
                    $cliente=null;

                    if(!$existe)
                    {
                        $cliente = new Clientes();

                        $cliente->userProfileId = $consulta_client['clientProfileData']['userProfileId'];
                        $cliente->nombre = $consulta_client['clientProfileData']['firstName'];
                        $cliente->apellido = $consulta_client['clientProfileData']['lastName'];
                        $cliente->email = $consulta_client['clientProfileData']['email'];
                    }

                    //agregamos el id del cliente en el pedido.
                    $pedidos->userProfileId = $consulta_client['clientProfileData']['userProfileId'];

                    //consultamos los productos de la venta
                    $consulta_items = Http::withHeaders([
                        'X-VTEX-API-AppToken' =>  $token,
                        'X-VTEX-API-AppKey' => $key
                    ])->get($api.'/'.$orderId);

                    //realizamos las carga de datos en la db en una transaccion atomica.
                    DB::transaction(function () use ($cliente,$pedidos,$consulta_items) {

                        if($cliente!=null)
                        {
                            $cliente->save();
                        }
                        $pedidos->save();
                        $pedidos->items()->createMany($consulta_items['items']);
                    });
                }
        }

        if(!$pedidos)
        {
            $this->error("Ocurrio un error");
        }

        $this->info("Datos importados con éxito");
        return 0;
    }
}
