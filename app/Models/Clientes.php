<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $primaryKey = 'userProfileId';
    public $incrementing = false;

    protected $table = 'clientes';
    protected $fillable = [
        'userProfileId',
        'nombre',
        'apellido',
        'email'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
