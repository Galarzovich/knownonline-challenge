<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $primaryKey = 'orderId';
    public $incrementing = false;

    protected $table = 'pedidos';
    protected $fillable = [
        'orderId',
        'paymentSystem',
        'monto_total'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    //relacion: 1 pedido tiene muchos items (productos)
    public function items()
    {
        return $this->hasMany(Items::class,'orderId');
    }



}
