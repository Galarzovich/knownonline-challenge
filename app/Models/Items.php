<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $primaryKey = 'uniqueId';
    public $incrementing = false;

    protected $table = 'items';
    protected $fillable = [
        'uniqueId',
        'refId',
        'productId',
        'name',
        'quantity'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];



}
