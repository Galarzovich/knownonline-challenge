<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('pedidos', function (Blueprint $table) {
            $table->string('orderId')->primary();
            $table->string('paymentSystem');
            $table->string('monto_total');
            $table->char('userProfileId',36);
            $table->foreign('userProfileId')->references('userProfileId')->on('clientes');
            $table->boolean('procesada')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
