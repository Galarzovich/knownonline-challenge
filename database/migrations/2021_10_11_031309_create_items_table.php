<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {

            $table->string('uniqueId')->primary();
            $table->string('refId')->nullable(); //si bien la documentacion no lo aclara, en la consulta hay un producto que tiene este campo vacio.
            $table->string('productId');
            $table->string('name');
            $table->integer('quantity');
            $table->string('orderId');
            $table->foreign('orderId')->references('orderId')->on('pedidos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
